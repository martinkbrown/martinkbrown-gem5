/*
 * Copyright (c) 2010-2012 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2001-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ron Dreslinski
 *          Ali Saidi
 *          Andreas Hansson
 */

#include "arch/registers.hh"
#include "config/the_isa.hh"
#include "debug/LLSC.hh"
#include "debug/MemoryAccess.hh"
#include "mem/abstract_mem.hh"
#include "mem/packet_access.hh"
#include "sim/system.hh"
#include "arch/stacktrace.hh"
#include "mem/abstract_mem_meta.hh"
#include "arch/utility.hh"
#include "debug/VirtualSegmentRefs.hh"

namespace MemoryMetaDataNamespace
{
   int max_print_index = 200;
   int max_printed_index = 0;
   int * printed_pid = new int[max_print_index];
   int printed_pid_i = 0;
   bool found_pid = false;
   Addr stackPtr = 0; 
   int pid;
   std::string pname;
   bool startMemStats = true;
}

using namespace MemoryMetaDataNamespace;

AbstractMemory::AbstractMemory(const Params *p) :
    MemObject(p), range(params()->range), pmemAddr(NULL),
    confTableReported(p->conf_table_reported), inAddrMap(p->in_addr_map),
    _system(NULL)
{
    if (size() % TheISA::PageBytes != 0)
        panic("Memory Size not divisible by page size\n");

}

void
AbstractMemory::setBackingStore(uint8_t* pmem_addr)
{
    pmemAddr = pmem_addr;

    metaPointers.init(range.size());
}

void
AbstractMemory::regStats()
{
    using namespace Stats;

    assert(system());
    metaPointers.avgCyclesToRead
        .name(name() + ".avgCyclesToRead")
        .desc("Average cycles between a write to address x and read from address x")
        .precision(2)
        ;

    metaPointers.numReads
        .name(name() + ".numReads")
        .desc("Number of reads to this memory")
        ;

    metaPointers.numWrites
        .name(name() + ".numWrites")
        .desc("Number of writes to this memory")
        ;

    metaPointers.numStackSegmentRefs
        .name(name() + ".numStackSegmentRefs")
        .desc("Number of memory references within the stack segment")
        ;

    metaPointers.percentageStackSegmentRefs
        .name(name() + ".percentageStackSegmentRefs")
        .desc("Percentage of memory references to the stack segment")
        .precision(2);

    metaPointers.percentageStackSegmentRefs = metaPointers.numStackSegmentRefs / (metaPointers.numReads + metaPointers.numWrites);

    metaPointers.numDataSegmentRefs
        .name(name() + ".numDataSegmentRefs")
        .desc("Number of memory references to the data segment")
        ;

    metaPointers.percentageDataSegmentRefs
        .name(name() + ".percentageDataSegmentRefs")
        .desc("Percentage of memory references to the data segment")
        .precision(2);

    metaPointers.percentageDataSegmentRefs = metaPointers.numDataSegmentRefs / (metaPointers.numReads + metaPointers.numWrites);;

    metaPointers.numHeapSegmentRefs
        .name(name() + ".numHeapSegmentRefs")
        .desc("Number of memory references to the heap segment")
        ;

    metaPointers.percentageHeapSegmentRefs
        .name(name() + ".percentageHeapSegmentRefs")
        .desc("Percentage of memory references within the heap segment")
        .precision(2);

    metaPointers.percentageHeapSegmentRefs = metaPointers.numHeapSegmentRefs / (metaPointers.numReads + metaPointers.numWrites);;

    metaPointers.numCodeSegmentRefs
        .name(name() + ".numCodeSegmentRefs")
        .desc("Number of memory references to the code segment")
        ;

    metaPointers.percentageCodeSegmentRefs
        .name(name() + ".percentageCodeSegmentRefs")
        .desc("Percentage of memory references to the code segment")
        .precision(2);

    metaPointers.percentageCodeSegmentRefs = metaPointers.numCodeSegmentRefs / (metaPointers.numReads + metaPointers.numWrites);;

    metaPointers.numBssSegmentRefs
        .name(name() + ".numBssSegmentRefs")
        .desc("Number of memory references to the BSS segment")
        ;

    metaPointers.percentageBssSegmentRefs
        .name(name() + ".percentageBssSegmentRefs")
        .desc("Percentage of memory references to the BSS segment")
        .precision(2);

    metaPointers.percentageBssSegmentRefs = metaPointers.numBssSegmentRefs / (metaPointers.numReads + metaPointers.numWrites);;

    metaPointers.numInterProcessReads
        .name(name() + ".numInterProcessReads")
        .desc("Number of reads that occurred by a different pid than the write-pid")
        ;

    metaPointers.percentageInterProcessReads
        .name(name() + ".percentageInterProcessReads")
        .desc("Percentage of memory reads read by a different pid")
        .precision(2)
        ;

    metaPointers.percentageInterProcessReads = metaPointers.numInterProcessReads / metaPointers.numReads;

    metaPointers.numInterCpuReads
        .name(name() + ".numInterCpuReads")
        .desc("Number of reads that occurred by a different core than the write-core")
        ;

    metaPointers.percentageInterCpuReads
        .name(name() + ".percentageInterCpuReads")
        .desc("Percentage of memory reads read by a different core")
        .precision(2)
        ;

    metaPointers.percentageInterCpuReads = metaPointers.numInterCpuReads / metaPointers.numReads;

    metaPointers.numSilentWrites
        .name(name() + ".numSilentWrites")
        .desc("Number of silent writes, writes that wrote the same value back to memory")
        ;

    metaPointers.percentageSilentWrites
        .name(name() + ".percentageSilentWrites")
        .desc("Percentage of writes that were silent")
        .precision(2)
        ;

    metaPointers.percentageSilentWrites = metaPointers.numSilentWrites / metaPointers.numReads;

    metaPointers.numZeroWrites
        .name(name() + ".numZeroWrites")
        .desc("Number of times memory was written with the value zero")
        ;

    metaPointers.numZeroReads
        .name(name() + ".numZeroReads")
        .desc("Number of times memory was read with the value zero")
        ;

    metaPointers.percentageZeroWrites
        .name(name() + ".percentageZeroWrites")
        .desc("Percentage of writes to memory of the value zero")
        .precision(2)
        ;

    metaPointers.percentageZeroWrites = metaPointers.numZeroWrites / metaPointers.numWrites;

    metaPointers.percentageZeroReads
        .name(name() + ".percentageZeroReads")
        .desc("Percentage of reads from memory of the value zero")
        .precision(2)
        ;

    metaPointers.percentageZeroReads = metaPointers.numZeroReads / metaPointers.numReads;

    metaPointers.histCyclesToRead
        .init(100)
        .name(name() + ".histTimeToRead")
        .desc("Number of times it took this number of cycles to for the first read to written data")
        ;

    bytesRead
        .init(system()->maxMasters())
        .name(name() + ".bytes_read")
        .desc("Number of bytes read from this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bytesRead.subname(i, system()->getMasterName(i));
    }
    bytesInstRead
        .init(system()->maxMasters())
        .name(name() + ".bytes_inst_read")
        .desc("Number of instructions bytes read from this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bytesInstRead.subname(i, system()->getMasterName(i));
    }
    bytesWritten
        .init(system()->maxMasters())
        .name(name() + ".bytes_written")
        .desc("Number of bytes written to this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bytesWritten.subname(i, system()->getMasterName(i));
    }
    numReads
        .init(system()->maxMasters())
        .name(name() + ".num_reads")
        .desc("Number of read requests responded to by this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        numReads.subname(i, system()->getMasterName(i));
    }
    numWrites
        .init(system()->maxMasters())
        .name(name() + ".num_writes")
        .desc("Number of write requests responded to by this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        numWrites.subname(i, system()->getMasterName(i));
    }
    numOther
        .init(system()->maxMasters())
        .name(name() + ".num_other")
        .desc("Number of other requests responded to by this memory")
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        numOther.subname(i, system()->getMasterName(i));
    }
    bwRead
        .name(name() + ".bw_read")
        .desc("Total read bandwidth from this memory (bytes/s)")
        .precision(0)
        .prereq(bytesRead)
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bwRead.subname(i, system()->getMasterName(i));
    }

    bwInstRead
        .name(name() + ".bw_inst_read")
        .desc("Instruction read bandwidth from this memory (bytes/s)")
        .precision(0)
        .prereq(bytesInstRead)
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bwInstRead.subname(i, system()->getMasterName(i));
    }
    bwWrite
        .name(name() + ".bw_write")
        .desc("Write bandwidth from this memory (bytes/s)")
        .precision(0)
        .prereq(bytesWritten)
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bwWrite.subname(i, system()->getMasterName(i));
    }
    bwTotal
        .name(name() + ".bw_total")
        .desc("Total bandwidth to/from this memory (bytes/s)")
        .precision(0)
        .prereq(bwTotal)
        .flags(total | nozero | nonan)
        ;
    for (int i = 0; i < system()->maxMasters(); i++) {
        bwTotal.subname(i, system()->getMasterName(i));
    }
    bwRead = bytesRead / simSeconds;
    bwInstRead = bytesInstRead / simSeconds;
    bwWrite = bytesWritten / simSeconds;
    bwTotal = (bytesRead + bytesWritten) / simSeconds;
}

AddrRange
AbstractMemory::getAddrRange() const
{
    return range;
}

// Add load-locked to tracking list.  Should only be called if the
// operation is a load and the LLSC flag is set.
void
AbstractMemory::trackLoadLocked(PacketPtr pkt)
{
    Request *req = pkt->req;
    Addr paddr = LockedAddr::mask(req->getPaddr());

    // first we check if we already have a locked addr for this
    // xc.  Since each xc only gets one, we just update the
    // existing record with the new address.
    list<LockedAddr>::iterator i;

    for (i = lockedAddrList.begin(); i != lockedAddrList.end(); ++i) {
        if (i->matchesContext(req)) {
            DPRINTF(LLSC, "Modifying lock record: context %d addr %#x\n",
                    req->contextId(), paddr);
            i->addr = paddr;
            return;
        }
    }

    // no record for this xc: need to allocate a new one
    DPRINTF(LLSC, "Adding lock record: context %d addr %#x\n",
            req->contextId(), paddr);
    lockedAddrList.push_front(LockedAddr(req));
}


// Called on *writes* only... both regular stores and
// store-conditional operations.  Check for conventional stores which
// conflict with locked addresses, and for success/failure of store
// conditionals.
bool
AbstractMemory::checkLockedAddrList(PacketPtr pkt)
{
    Request *req = pkt->req;
    Addr paddr = LockedAddr::mask(req->getPaddr());
    bool isLLSC = pkt->isLLSC();

    // Initialize return value.  Non-conditional stores always
    // succeed.  Assume conditional stores will fail until proven
    // otherwise.
    bool allowStore = !isLLSC;

    // Iterate over list.  Note that there could be multiple matching records,
    // as more than one context could have done a load locked to this location.
    // Only remove records when we succeed in finding a record for (xc, addr);
    // then, remove all records with this address.  Failed store-conditionals do
    // not blow unrelated reservations.
    list<LockedAddr>::iterator i = lockedAddrList.begin();

    if (isLLSC) {
        while (i != lockedAddrList.end()) {
            if (i->addr == paddr && i->matchesContext(req)) {
                // it's a store conditional, and as far as the memory system can
                // tell, the requesting context's lock is still valid.
                DPRINTF(LLSC, "StCond success: context %d addr %#x\n",
                        req->contextId(), paddr);
                allowStore = true;
                break;
            }
            // If we didn't find a match, keep searching!  Someone else may well
            // have a reservation on this line here but we may find ours in just
            // a little while.
            i++;
        }
        req->setExtraData(allowStore ? 1 : 0);
    }
    // LLSCs that succeeded AND non-LLSC stores both fall into here:
    if (allowStore) {
        // We write address paddr.  However, there may be several entries with a
        // reservation on this address (for other contextIds) and they must all
        // be removed.
        i = lockedAddrList.begin();
        while (i != lockedAddrList.end()) {
            if (i->addr == paddr) {
                DPRINTF(LLSC, "Erasing lock record: context %d addr %#x\n",
                        i->contextId, paddr);
                i = lockedAddrList.erase(i);
            } else {
                i++;
            }
        }
    }

    return allowStore;
}


#if TRACING_ON

#define CASE(A, T)                                                      \
  case sizeof(T):                                                       \
    DPRINTF(MemoryAccess,"%s of size %i on address 0x%x data 0x%x\n",   \
            A, pkt->getSize(), pkt->getAddr(), pkt->get<T>());          \
  break


#define TRACE_PACKET(A)                                                 \
    do {                                                                \
        switch (pkt->getSize()) {                                       \
          CASE(A, uint64_t);                                            \
          CASE(A, uint32_t);                                            \
          CASE(A, uint16_t);                                            \
          CASE(A, uint8_t);                                             \
          default:                                                      \
            DPRINTF(MemoryAccess, "%s of size %i on address 0x%x\n",    \
                    A, pkt->getSize(), pkt->getAddr());                 \
            DDUMP(MemoryAccess, pkt->getPtr<uint8_t>(), pkt->getSize());\
        }                                                               \
    } while (0)

#else

#define TRACE_PACKET(A)

#endif

void
AbstractMemory::access(PacketPtr pkt)
{
    assert(AddrRange(pkt->getAddr(),
                     pkt->getAddr() + pkt->getSize() - 1).isSubset(range));

    if (pkt->memInhibitAsserted()) {
        DPRINTF(MemoryAccess, "mem inhibited on 0x%x: not responding\n",
                pkt->getAddr());
        return;
    }

    uint8_t *hostAddr = pmemAddr + pkt->getAddr() - range.start();
    uint64_t offset = pkt->getAddr() - range.start();
    offset = offset;

    if (pkt->cmd == MemCmd::SwapReq) {
        TheISA::IntReg overwrite_val;
        bool overwrite_mem;
        uint64_t condition_val64;
        uint32_t condition_val32;

        if (!pmemAddr)
            panic("Swap only works if there is real memory (i.e. null=False)");
        assert(sizeof(TheISA::IntReg) >= pkt->getSize());

        overwrite_mem = true;
        // keep a copy of our possible write value, and copy what is at the
        // memory address into the packet
        std::memcpy(&overwrite_val, pkt->getPtr<uint8_t>(), pkt->getSize());
        std::memcpy(pkt->getPtr<uint8_t>(), hostAddr, pkt->getSize());

        if (pkt->req->isCondSwap()) {
            if (pkt->getSize() == sizeof(uint64_t)) {
                condition_val64 = pkt->req->getExtraData();
                overwrite_mem = !std::memcmp(&condition_val64, hostAddr,
                                             sizeof(uint64_t));
            } else if (pkt->getSize() == sizeof(uint32_t)) {
                condition_val32 = (uint32_t)pkt->req->getExtraData();
                overwrite_mem = !std::memcmp(&condition_val32, hostAddr,
                                             sizeof(uint32_t));
            } else
                panic("Invalid size for conditional read/write\n");
        }

        if (overwrite_mem)
            std::memcpy(hostAddr, &overwrite_val, pkt->getSize());

        assert(!pkt->req->isInstFetch());
        TRACE_PACKET("Read/Write");
        numOther[pkt->req->masterId()]++;
    } else if (pkt->isRead()) {
        assert(!pkt->isWrite());
        if (pkt->isLLSC()) {
            trackLoadLocked(pkt);
        }
        if (pmemAddr)
            memcpy(pkt->getPtr<uint8_t>(), hostAddr, pkt->getSize());
        TRACE_PACKET(pkt->req->isInstFetch() ? "IFetch" : "Read");
        numReads[pkt->req->masterId()]++;
        bytesRead[pkt->req->masterId()] += pkt->getSize();
        if (pkt->req->isInstFetch())
            bytesInstRead[pkt->req->masterId()] += pkt->getSize();
    } else if (pkt->isWrite()) {
        if (writeOK(pkt)) {
            if (pmemAddr) {
                memcpy(hostAddr, pkt->getPtr<uint8_t>(), pkt->getSize());
                DPRINTF(MemoryAccess, "%s wrote %x bytes to address %x\n",
                        __func__, pkt->getSize(), pkt->getAddr());

            }
            assert(!pkt->req->isInstFetch());
            TRACE_PACKET("Write");
            numWrites[pkt->req->masterId()]++;
            bytesWritten[pkt->req->masterId()] += pkt->getSize();
        }
    } else if (pkt->isInvalidate()) {
        // no need to do anything
    } else {
        panic("unimplemented");
    }

    //Maybe do this per block address
    //http://www.mail-archive.com/gem5-users@gem5.org/msg05630.html
    // masterId = 8 means that this is reading for data (as opposed to instructions)
    // printing system()->getMasterName(pkt->req->masterId()) would print "cpu.data"

    //if(system() && pkt->isRequest() && pkt->req->hasVaddr() && pkt->req->hasContextId() && pkt->req->masterId() == 8 && pkt->req->hasPC() && (pkt->isRead() || (pkt->isWrite() && writeOK(pkt)) )) {
    if(pkt->req->hasVaddr() && pkt->req->hasContextId() && system() && 
       !pkt->req->isInstFetch() && pkt->req->hasPC() && 
       (pkt->isRead() || pkt->isWrite())) { 


        // We need to have a ThreadContext in order to get process information
        ThreadContext *tc = system()->getThreadContext(pkt->req->threadId());

         pid = system()->getPid(tc);
         pname = system()->getPname(tc);

        if(system()->simThisMode(tc)) {

            stackPtr = tc->readIntReg(TheISA::StackPointerReg2);

            if(pid > 0 && system()->simThisProcess(pname))
            {
                metaPointers.initMetaData(offset, pkt->getAddr());

                metaPointers.setCpuId(offset, pkt->req->contextId(), pkt->isWrite());
                metaPointers.setProcessId(offset, pid, pname, pkt->isWrite());
                metaPointers.setThreadId(offset, pkt->req->threadId(), pkt->isWrite());
                metaPointers.setProgramCounter(offset, pkt->req->getPC(), pkt->isWrite());
                metaPointers.setCycle(offset, pkt->req->time(), pkt->isWrite());
                metaPointers.setAddressSpaceId(offset, pkt->req->getAsid(), pkt->isWrite());
                metaPointers.setVirtualAddress(offset, pkt->req->getVaddr(), stackPtr, pkt->isWrite());
                metaPointers.incrementRefs(offset, pkt->isWrite());

                if(pkt->isWrite()) {
                    metaPointers.setData(offset, pkt->getPtr<uint8_t>(), pkt->getSize(), pkt->isWrite());
                }

                Addr stackPtr;
 
                // Find the stackPtr for this thread, I suppose
                stackPtr = tc->readIntReg(TheISA::StackPointerReg2);
         
                // Now let's get the ProcessInfo, which includes process name and pid
                TheISA::ProcessInfo * procInfo = new TheISA::ProcessInfo(tc);
                procInfo->getmm(stackPtr);
        
                uint32_t start_code = procInfo->startCode(stackPtr);
                uint32_t end_code = procInfo->endCode(stackPtr); 
                uint32_t start_stack = procInfo->startStack(stackPtr);
                uint32_t brk = procInfo->endHeap(stackPtr);
                uint32_t start_brk = procInfo->startHeap(stackPtr);
                uint32_t end_data = procInfo->endData(stackPtr);
                uint32_t start_data = procInfo->startData(stackPtr);

                string sregion = " ";
                uint32_t start;
                uint32_t end;

                Addr vaddr = pkt->req->getVaddr();

                if(vaddr >= start_stack && vaddr <= stackPtr) {
                    start = start_stack;
                    end = stackPtr;
                    sregion = "Stack  ";
                }   
                else if(vaddr >= start_brk && vaddr <= brk) {
                    start = start_brk;
                    end = brk;
                    sregion = "Heap   ";
                }   
                else if(vaddr >= start_data && vaddr <= end_data) {
                    start = start_data;
                    end = end_data;
                    sregion = "Data   ";
                }   
                else if(vaddr >= start_code && vaddr <= end_code) {
                    start = start_code;
                    end = end_code;
                    sregion = "Code   ";
                }
                else if(vaddr >= end_data && vaddr <= start_brk) {
                    start = end_data;
                    end = start_brk;
                    sregion = "BSS    ";
                }

                metaPointers.setVirtualAddress(offset, vaddr, pkt->isWrite(), sregion[0]);

                if(sregion == " ")
                    sregion = "Unknown";

                start = start; end = end; // gem5.fast complains if I don't use start and end

                DPRINTF(VirtualSegmentRefs, "process %d (%s) making memory %s %s request to the %s segment [%u\t@ %d\t %u]\t(physical @ %d)\n", 
                    pid, pname, pkt->req->isInstFetch() ? "instruction" : "data", pkt->isWrite() ? "write" : "read ", sregion, start, pkt->req->getVaddr(), end, pkt->getAddr());
            }

        }
    }

    if (pkt->needsResponse()) {
        pkt->makeResponse();
    }
}

void
AbstractMemory::functionalAccess(PacketPtr pkt)
{
    assert(AddrRange(pkt->getAddr(),
                     pkt->getAddr() + pkt->getSize() - 1).isSubset(range));

    uint8_t *hostAddr = pmemAddr + pkt->getAddr() - range.start();

    if (pkt->isRead()) {
        if (pmemAddr)
            memcpy(pkt->getPtr<uint8_t>(), hostAddr, pkt->getSize());
        TRACE_PACKET("Read");
        pkt->makeResponse();
    } else if (pkt->isWrite()) {
        if (pmemAddr)
            memcpy(hostAddr, pkt->getPtr<uint8_t>(), pkt->getSize());
        TRACE_PACKET("Write");
        pkt->makeResponse();
    } else if (pkt->isPrint()) {
        Packet::PrintReqState *prs =
            dynamic_cast<Packet::PrintReqState*>(pkt->senderState);
        assert(prs);
        // Need to call printLabels() explicitly since we're not going
        // through printObj().
        prs->printLabels();
        // Right now we just print the single byte at the specified address.
        ccprintf(prs->os, "%s%#x\n", prs->curPrefix(), *hostAddr);
    } else {
        panic("AbstractMemory: unimplemented functional command %s",
              pkt->cmdString());
    }
}
