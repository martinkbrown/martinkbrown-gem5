/*
 * Copyright (c) 2012 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2001-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHEisWriteISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Martin Brown 
 */

/**
 * @file
 * AbstractMemory declaration
 */

#ifndef __ABSTRACT_MEMORY_META_HH__
#define __ABSTRACT_MEMORY_META_HH__

#include <string>
#include "params/AbstractMemory.hh"
#include "arch/stacktrace.hh"
#include "sim/stats.hh"

#define TEMP_PRINTED_SIZE 200

namespace MemoryMetaDataNamespace
{
    extern int max_print_index;
    extern int max_printed_index;
    extern int * printed_pid;
    extern int printed_pid_i;
    extern bool found_pid;
    extern Addr stackPtr;
    extern bool startMemStats;
}

using namespace std;

struct ReadWriteMeta {
    Addr pc;
    Addr virtualAddress;

    int pid;
    string pname;
    int cpuid;
    int tid;
    int asid;
    Tick tick;
    int numRefs;
    uint64_t cycle;
    int inUserMode;
};

struct MemoryMetaData {

    Addr physicalAddress;

    uint8_t* data;

    ReadWriteMeta read;
    ReadWriteMeta write;

    int timeToRead;

    uint64_t mem_def_cycle;
    uint64_t mem_use_cycle;
};

class MetaPointers {

    public:

        enum { READ = 1, WRITE};
        enum { STACK, HEAP, DATA, INST};
        uint64_t size;
        MemoryMetaData ** meta;

        int numwtr;
        Tick sumwtr;

        Stats::Scalar numReads;
        Stats::Scalar numWrites;

        Stats::Average avgCyclesToRead;
        Stats::SparseHistogram histCyclesToRead;

        Stats::Scalar numInterProcessReads;
        Stats::Formula percentageInterProcessReads;
/*
        Stats::Scalar numInterProcessUsermodeReads;
        Stats::Formula percentageInterProcessUsermodeReads;
*/
        Stats::Scalar numInterCpuReads;
        Stats::Formula percentageInterCpuReads;

        Stats::Scalar numStackSegmentRefs;
        Stats::Formula percentageStackSegmentRefs;

        Stats::Scalar numDataSegmentRefs;
        Stats::Formula percentageDataSegmentRefs;

        Stats::Scalar numHeapSegmentRefs;
        Stats::Formula percentageHeapSegmentRefs;

        Stats::Scalar numCodeSegmentRefs;
        Stats::Formula percentageCodeSegmentRefs;

        Stats::Scalar numBssSegmentRefs; 
        Stats::Formula percentageBssSegmentRefs;

        Stats::Scalar numSilentWrites;
        Stats::Formula percentageSilentWrites;

        Stats::Scalar numZeroWrites;
        Stats::Scalar numZeroReads;
        Stats::Formula percentageZeroWrites;
        Stats::Formula percentageZeroReads;

        MetaPointers();
        MetaPointers(int memSize);
        ~MetaPointers();

        void init(int memSize);
        void initMetaData(uint64_t offset, Addr addr);

        void setData(uint64_t offset, uint8_t* data, int size, int isWrite);
        void setCpuId(uint64_t offset, int cpuid, int isWrite);
        void setProcessId(uint64_t offset, int pid, string pname, int isWrite);
        void setThreadId(uint64_t offset, int tid, int isWrite);
        void setProgramCounter(uint64_t offset, Addr addr, int isWrite);
        void setCycle(uint64_t offset, Tick tick, int isWrite);
        void setAddressSpaceId(uint64_t offset, int asid, int isWrite);
        void setVirtualAddress(uint64_t offset, Addr addr, int isWrite, char region);
        void setMemDefCycle(uint64_t offset, uint64_t cycle);
        void setMemUseCycle(uint64_t offset, uint64_t cycle);

        void incrementRefs(uint64_t offset, int isWrite);

        int getLastOp(uint64_t offset);

        Addr getPhysicalAddress(uint64_t offset);
        Addr getLastReadPc(uint64_t offset);
        Addr getLastWritePc(uint64_t offset);
        Addr getLastPc(uint64_t offset);

        int getLastReadPid(uint64_t offset);
        int getLastWritePid(uint64_t offset);
        int getLastPid(uint64_t offset);

        int getLastReadTid(uint64_t offset);
        int getLastWriteTid(uint64_t offset);
        int getLastTid(uint64_t offset);

        int getLastCycle(uint64_t offset);

        int getNumReads(uint64_t offset);
        int getNumReads();
        int getNumWrites(uint64_t offset);
        int getNumWrites();

        int getNumRefs(uint64_t offset);
        int getNumRefs();
        uint64_t getMemDefCycle(uint64_t offset);
        uint64_t getMemUseCycle(uint64_t offset);

        Tick getTimeToRead(uint64_t offset);

        int getAddressSpaceId(uint64_t offset);

        int hasBeenWritten(uint64_t offset);

        void print(uint64_t offset);
};

#endif //ABSTRACT_MEM_META_HH
