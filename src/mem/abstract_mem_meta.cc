/*
 * Copyright (c) 2010-2012 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2001-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ron Dreslinski
 *          Ali Saidi
 *          Andreas Hansson
 */

#include <string>
#include <fstream>
#include <sstream>
#include "sim/system.hh"
#include "abstract_mem_meta.hh"
#include "debug/MemoryMetaData.hh"

#define TEMP_PRINTED_SIZE 200

using namespace std;

MetaPointers::MetaPointers() {}

MetaPointers::MetaPointers(int memSize) {

    init(memSize);

}

MetaPointers::~MetaPointers() {

    // free up some space
    for(int i = 0; i < size; i++) {
        if(meta[i])
            delete meta[i];
    }

    delete [] meta;
}

void MetaPointers::init(int memSize) {


    if(!meta) {
       size = memSize;
       numwtr = 0;
       sumwtr = 0;

       numReads = 0;
       numWrites = 0;

       meta = new MemoryMetaData*[size];
    }
}

void MetaPointers::initMetaData(uint64_t offset, Addr addr) {

   if(meta == NULL) {
      panic("meta not initialized\n"); 
      return;
   }

   if(offset > size) {
       cout << "offset > size " << offset << " > " << size << endl;
       return;
   }

   if(!meta[offset]) {
       meta[offset] = new MemoryMetaData;
       memset(meta[offset], 0, sizeof(MemoryMetaData));
       meta[offset]->mem_def_cycle = 0;
       meta[offset]->mem_use_cycle = 0;
   }

   meta[offset]->physicalAddress = addr;
}

void MetaPointers::setData(uint64_t offset, uint8_t* data, int size, int isWrite) {

    if(meta[offset]->data) {

        if((*meta[offset]->data) == (*data)) {

            numSilentWrites++;
        }

        if(*(data) == 0) {

            if(isWrite) {
                numZeroWrites++;
            }
            else
                numZeroReads++;

        }

        delete meta[offset]->data;

    }

    meta[offset]->data = new uint8_t[size];
    memcpy((void*) meta[offset]->data, (void *) data, size);
}

void MetaPointers::setCpuId(uint64_t offset, int cpuid, int isWrite) {

    if(!isWrite) {

        meta[offset]->read.cpuid = cpuid;

        if(hasBeenWritten(offset) && meta[offset]->write.cpuid != cpuid) {

            numInterCpuReads++;

            DPRINTF(MemoryMetaDataSharedMemoryAccess, "shmem read: Core %d wrote to %d and core %d read from it\n",
                meta[offset]->write.cpuid,
                offset,
                cpuid);

        }
    }
    else {

        meta[offset]->write.cpuid = cpuid;

    }

}

void MetaPointers::setProcessId(uint64_t offset, int pid, string pname, int isWrite) {

    if(!meta[offset]) return;

    if(!isWrite) {

        meta[offset]->read.pid = pid;
        // meta[offset]->read.pname = pname;
        // inter-process communication

        if(hasBeenWritten(offset) && meta[offset]->write.pid != pid) {

            numInterProcessReads++;
            DPRINTF(MemoryMetaDataIpc, "IPC read: (%d) wrote to %d and (%d) read from it\n",
                meta[offset]->write.pid,
                offset,
                pid);
        }
    }
    else {
        meta[offset]->write.pid = pid;
    }
}

void MetaPointers::setThreadId(uint64_t offset, int tid, int isWrite) {

    if(tid > 0) {
        DPRINTF(MemoryMetaData, "miracle, tid = %d\n", tid);
    }

    if(!isWrite)
        meta[offset]->read.tid = tid;
    else
        meta[offset]->write.tid = tid;

}

void MetaPointers::setProgramCounter(uint64_t offset, Addr addr, int isWrite) {

    if(!isWrite)
        meta[offset]->read.pc = addr;
    else
        meta[offset]->write.pc = addr;
}

void MetaPointers::setCycle(uint64_t offset, Tick tick, int isWrite) {

    int cycle = tick / 500;

    if(!isWrite) {

        // valid?
        if(cycle > meta[offset]->write.cycle) {

           // if this data has been written but not yet read
            if(meta[offset]->read.cycle < meta[offset]->write.cycle) {

                meta[offset]->timeToRead = (cycle - meta[offset]->write.cycle);

                if(meta[offset]->timeToRead > 0) {

                    avgCyclesToRead = numwtr == 0 ? 0 : sumwtr / numwtr;

                    if(meta[offset]->timeToRead < 100) {

                        numwtr++;
                        sumwtr+= meta[offset]->timeToRead;
                        histCyclesToRead.sample(meta[offset]->timeToRead);
                    }
                }
            }
        }

        meta[offset]->read.cycle = cycle;
    }
    else
        meta[offset]->write.cycle = cycle;

}

void MetaPointers::setAddressSpaceId(uint64_t offset, int asid, int isWrite) {

    if(!isWrite)
        meta[offset]->read.asid = asid;
    else
        meta[offset]->write.asid = asid;
}

void MetaPointers::setVirtualAddress(uint64_t offset, Addr addr, int isWrite, char region) {

    if(!isWrite)
        meta[offset]->read.virtualAddress = addr;
    else
        meta[offset]->write.virtualAddress = addr;

    switch(region) {

        case 'S':
            numStackSegmentRefs++;
        break;

        case 'H':
            numHeapSegmentRefs++;
        break;

        case 'D':
            numDataSegmentRefs++;
        break;

        case 'C':
            numCodeSegmentRefs++;
        break;

        case 'B':
            numBssSegmentRefs++;
        break;
    }

}

void MetaPointers::incrementRefs(uint64_t offset, int isWrite) {

    if(!isWrite) {
        numReads++;
        meta[offset]->read.numRefs++;

    }
    else {
        numWrites++;
        meta[offset]->write.numRefs++;

    }
}


void MetaPointers::setMemDefCycle(uint64_t offset, uint64_t cycle)
{
    if(meta[offset])
        meta[offset]->mem_def_cycle = cycle;
    else
        panic("meta[offset] not initialized");
}

void MetaPointers::setMemUseCycle(uint64_t offset, uint64_t cycle)
{
    if(meta[offset])
        meta[offset]->mem_use_cycle = cycle;
    else
        panic("meta[offset] not initialized");
}

int MetaPointers::getLastOp(uint64_t offset) {

     int op = WRITE;

     if(meta[offset]) {

         if(meta[offset]->read.cycle > meta[offset]->write.cycle)
             op = READ;
         else
             op = WRITE;
     }
     else {
         perror("Tried to get last op but this offset has not been initialized it has not been initialized. ");
         exit(0);
     }

     return op;
}

Addr MetaPointers::getPhysicalAddress(uint64_t offset)    { return meta[offset] ? meta[offset]->physicalAddress : 0; }
Addr MetaPointers::getLastReadPc(uint64_t offset)         { return meta[offset] ? meta[offset]->read.pc : 0; }
Addr MetaPointers::getLastWritePc(uint64_t offset)        { return meta[offset] ? meta[offset]->write.pc : 0; }
Addr MetaPointers::getLastPc(uint64_t offset)             { return getLastOp(offset) == READ ? getLastReadPc(offset) : getLastWritePc(offset); }

int MetaPointers::getLastReadPid(uint64_t offset)         { return meta[offset] ? meta[offset]->read.pid : 0; }
int MetaPointers::getLastWritePid(uint64_t offset)        { return meta[offset] ? meta[offset]->write.pid : 0; }
int MetaPointers::getLastPid(uint64_t offset)             { return getLastOp(offset) == READ ? getLastReadPid(offset) : getLastWritePid(offset); }

int MetaPointers::getLastReadTid(uint64_t offset)         { return meta[offset] ? meta[offset]->read.tid : 0; }
int MetaPointers::getLastWriteTid(uint64_t offset)        { return meta[offset] ? meta[offset]->write.tid : 0; }
int MetaPointers::getLastTid(uint64_t offset)             { return getLastOp(offset) == READ ? getLastReadTid(offset) : getLastWriteTid(offset); }

int MetaPointers::getLastCycle(uint64_t offset)           { return meta[offset] ? std::max(meta[offset]->read.cycle, meta[offset]->write.cycle) : 0; }

int MetaPointers::getNumReads(uint64_t offset)            { return meta[offset] ? meta[offset]->read.numRefs : 0;  }
int MetaPointers::getNumReads()                      { return numReads.value(); }
int MetaPointers::getNumWrites(uint64_t offset)           { return meta[offset] ? meta[offset]->write.numRefs : 0; }
int MetaPointers::getNumWrites()                     { return numWrites.value(); }

int MetaPointers::getNumRefs(uint64_t offset)             { return meta[offset] ? meta[offset]->read.numRefs + meta[offset]->write.numRefs : 0; }
int MetaPointers::getNumRefs()                       { return getNumReads() + getNumWrites(); }

Tick MetaPointers::getTimeToRead(uint64_t offset)         { return meta[offset] ? meta[offset]->timeToRead : 0; }

int MetaPointers::getAddressSpaceId(uint64_t offset)      { return getLastOp(offset) == 0 ? meta[offset]->read.asid : meta[offset]->write.asid; }

// pid will be 0 (false) if nothing has yet been written
int MetaPointers::hasBeenWritten(uint64_t offset)         { return meta[offset] ? meta[offset]->write.pid > 0 : 0; }
uint64_t MetaPointers::getMemDefCycle(uint64_t offset)    { return meta[offset] && offset < size ? meta[offset]->mem_def_cycle : 0; }
uint64_t MetaPointers::getMemUseCycle(uint64_t offset)    { return meta[offset] && offset < size ? meta[offset]->mem_use_cycle : 0; }

void MetaPointers::print(uint64_t offset) {

    if(!meta) return;
    if(!meta[offset]) return;

    cout << "meta[" << offset << "]." << endl;
    cout << "\taddress " << getPhysicalAddress(offset) << endl;
    cout << "\tread pid = " << getLastReadPid(offset) << endl;
    cout << "\twrite pid = " << getLastWritePid(offset) << endl;
    cout << "\tread PC = " << getLastReadPc(offset) << endl;
    cout << "\twrite PC = " << getLastWritePc(offset) << endl;
    cout << "\tnum reads = " << getNumReads(offset) << endl;
    cout << "\tnum writes = " << getNumWrites(offset) << endl;
    cout << "\tnum refs = " << getNumRefs(offset) << endl;
    cout << "\twritetoread = " << getTimeToRead(offset) << endl;
}
