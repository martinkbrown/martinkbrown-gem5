/*
 * Copyright (c) 2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Nathan Binkert
 */

#include <string>

#include "arch/arm/isa_traits.hh"
#include "arch/arm/stacktrace.hh"
#include "arch/arm/vtophys.hh"
#include "base/bitfield.hh"
#include "base/trace.hh"
#include "cpu/base.hh"
#include "cpu/thread_context.hh"
#include "mem/fs_translating_port_proxy.hh"
#include "sim/system.hh"

using namespace std;
namespace ArmISA
{
    ProcessInfo::ProcessInfo(ThreadContext *_tc)
        : tc(_tc)
    {
        Addr addr = 0;

        

        FSTranslatingPortProxy &vp = tc->getVirtProxy();

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("thread_info_size", addr))
            panic("thread info not compiled into kernel\n");

        thread_info_size = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("task_struct_size", addr))
            panic("task_struct_size compiled into kernel\n");

        task_struct_size = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("thread_info_task", addr))
            panic("thread_info_task not compiled into kernel\n");

        task_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("task_struct_pid", addr))
            panic("task_struct_pid not compiled into kernel\n");

        pid_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("task_struct_comm", addr))
            panic("task_struct_comm not compiled into kernel\n");

        name_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_size", addr))
            panic("mm_struct_size not compiled into kernel\n");

        mm_struct_size = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("task_struct_mm", addr))
            panic("task_struct_mm not compiled into kernel\n");

        mm_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_mmap", addr))
            panic("mm_struct_mmap not compiled into kernel\n");

        mmap_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_area_struct_size", addr))
            panic("vm_area_struct_size not compiled into kernel\n");

        vm_area_struct_size = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_area_struct_vm_start", addr))
            panic("vm_area_struct_vm_start not compiled into kernel\n");

        vm_start_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_area_struct_vm_end", addr))
            panic("vm_area_struct_vm_end not compiled into kernel\n");

        vm_end_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_area_struct_vm_next", addr))
            panic("vm_area_struct_vm_next not compiled into kernel\n");

        vm_next_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_start_code", addr))
            panic("mm_struct_start_code not compiled into kernel\n");

        start_code_off = vp.readGtoH<int32_t>(addr);

         
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_end_code", addr))
            panic("mm_struct_end_code not compiled into kernel\n");

        end_code_off = vp.readGtoH<int32_t>(addr);

        
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_start_brk", addr))
            panic("mm_struct_start_brk not compiled into kernel\n");

        start_brk_off = vp.readGtoH<int32_t>(addr);

         
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_brk", addr))
            panic("mm_struct_brk not compiled into kernel\n");

        brk_off = vp.readGtoH<int32_t>(addr);

        
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_start_stack", addr))
            panic("mm_struct_start_stack not compiled into kernel\n");

        start_stack_off = vp.readGtoH<int32_t>(addr);

        
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_start_data", addr))
            panic("mm_struct_start_data not compiled into kernel\n");

        start_data_off = vp.readGtoH<int32_t>(addr);

        
        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_end_data", addr))
            panic("mm_struct_end_data not compiled into kernel\n");

        end_data_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("mm_struct_stack_vm", addr))
            panic("mm_struct_stack_vm not compiled into kernel\n");

        stack_vm_off = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_growsup", addr)) 
            panic("vm_growsup not compiled into kernel\n");

        vm_growsup = vp.readGtoH<int32_t>(addr);

        if (!tc->getSystemPtr()->kernelSymtab->findAddress("vm_growsdown", addr))
            panic("vm_growsdown not compiled into kernel\n");

        vm_growsdown = vp.readGtoH<int32_t>(addr);

        _mm = -1;

    }

    Addr
    ProcessInfo::task(Addr ksp) const
    {
        Addr base = ksp & ~0x1fff;
        if (base == ULL(0xffffffffc0000000))
            return 0;

        Addr tsk;

        FSTranslatingPortProxy &vp = tc->getVirtProxy();
        tsk = vp.readGtoH<Addr>(base + task_off);

        return tsk;
    }

    int
    ProcessInfo::pid(Addr ksp) const
    {
        Addr task = this->task(ksp);
        if (!task)
            return -1;

        uint16_t pd;

        FSTranslatingPortProxy &vp = tc->getVirtProxy();
        pd = vp.readGtoH<uint16_t>(task + pid_off);

        return pd;
    }

    string
    ProcessInfo::name(Addr ksp) const
    {
        Addr task = this->task(ksp);
        if (!task)
            return "unknown";

        char comm[256];
        CopyStringOut(tc, comm, task + name_off, sizeof(comm));
        if (!comm[0])
            return "startup";

        return comm;
    }

    /** 
     * Get boundaries for all of the memory segments, so that we don't have to
     * call a routine for each one
    */
    void 
    ProcessInfo::getmm(Addr ksp) 
    {
        Addr task = this->task(ksp);
        if (!task)
            return;

        if(_mm != -1)
            return;

        FSTranslatingPortProxy & vp = tc->getVirtProxy();
        _mm = vp.readGtoH<Addr>(task + mm_off);
        _start_code = vp.readGtoH<uint32_t>(_mm + start_code_off);
        _end_code = vp.readGtoH<uint32_t>(_mm + end_code_off);
        _start_brk = vp.readGtoH<uint32_t>(_mm + start_brk_off);
        _end_brk = vp.readGtoH<uint32_t>(_mm + brk_off);
        _start_stack = vp.readGtoH<uint32_t>(_mm + start_stack_off);
        _start_data = vp.readGtoH<uint32_t>(_mm + start_data_off);
        _end_data = vp.readGtoH<uint32_t>(_mm + end_data_off);
        _vm_grows = vp.readGtoH<uint32_t>(_mm + stack_vm_off); 

        _vm_area = vp.readGtoH<uint64_t>(_mm + mmap_off);
        _vm_area_start = vp.readGtoH<uint32_t>(_vm_area + vm_start_off);
        _vm_area_end = vp.readGtoH<uint32_t>(_vm_area + vm_end_off);
    }

    /**
     * Get vm_area_structs. This is similar to what /proc/{pid}/maps displays
     */
    void
    ProcessInfo::vmAreaNext()
    {
        FSTranslatingPortProxy &vp = tc->getVirtProxy();
        _vm_area = vp.readGtoH<Addr>(_vm_area + vm_next_off); 
        _vm_area_start = vp.readGtoH<uint32_t>(_vm_area + vm_start_off);
        _vm_area_end = vp.readGtoH<uint32_t>(_vm_area + vm_end_off);
    }

    uint32_t 
    ProcessInfo::vmAreaStart()
    {
        return _vm_area_start;
    }

    uint32_t
    ProcessInfo::vmAreaEnd()
    {
        return _vm_area_end;
    }

    void
    ProcessInfo::printVmRanges(Addr ksp)
    {
       getmm(ksp);

       while(_vm_area != 0) {

           printf("[%u - %u]", _vm_area_start, _vm_area_end);
           vmAreaNext();

       }

       printf("\n");
    }

    uint32_t
    ProcessInfo::startCode(Addr ksp) const
    {
        return _start_code;
    }

    uint32_t
    ProcessInfo::endCode(Addr ksp) const
    {   
        return _end_code;
    }

    uint32_t
    ProcessInfo::startHeap(Addr ksp) const
    {
        return _start_brk;
    }

    uint32_t
    ProcessInfo::endHeap(Addr ksp) const
    {
        return _end_brk;
    }

    uint32_t
    ProcessInfo::startStack(Addr ksp) const
    {
        return _start_stack;
    }


    uint32_t
    ProcessInfo::startData(Addr ksp) const
    {
        return _start_data;
    }


    uint32_t
    ProcessInfo::endData(Addr ksp) const
    {
        return _end_data;
    }

    int
    ProcessInfo::stackGrowsUp(Addr ksp) const
    {   
        return _vm_grows == vm_growsup;
    }

    StackTrace::StackTrace()
        : tc(0), stack(64)
    {
    }

    StackTrace::StackTrace(ThreadContext *_tc, StaticInstPtr inst)
        : tc(0), stack(64)
    {
        trace(_tc, inst);
    }

    StackTrace::~StackTrace()
    {
    }

    void
    StackTrace::trace(ThreadContext *_tc, bool is_call)
    {
    }

    bool
    StackTrace::isEntry(Addr addr)
    {
        return false;
    }

    bool
    StackTrace::decodeStack(MachInst inst, int &disp)
    {
        return false;
    }

    bool
    StackTrace::decodeSave(MachInst inst, int &reg, int &disp)
    {
        return false;
    }

    /*
     * Decode the function prologue for the function we're in, and note
     * which registers are stored where, and how large the stack frame is.
     */
    bool
    StackTrace::decodePrologue(Addr sp, Addr callpc, Addr func,
                               int &size, Addr &ra)
    {
        return false;
    }

#if TRACING_ON
    void
    StackTrace::dump()
    {
        DPRINTFN("------ Stack ------\n");
    
        DPRINTFN(" Not implemented\n");
    }
#endif
}
