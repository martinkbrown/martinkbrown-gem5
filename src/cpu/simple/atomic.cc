/*
 * Copyright (c) 2012 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2002-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Steve Reinhardt
 */

#include "arch/stacktrace.hh"
#include "arch/locked_mem.hh"
#include "arch/mmapped_ipr.hh"
#include "arch/utility.hh"
#include "base/bigint.hh"
#include "base/output.hh"
#include "config/the_isa.hh"
#include "cpu/simple/atomic.hh"
#include "cpu/exetrace.hh"
#include "debug/Drain.hh"
#include "debug/ExecFaulting.hh"
#include "debug/SimpleCPU.hh"
#include "mem/packet.hh"
#include "mem/packet_access.hh"
#include "mem/physical.hh"
#include "params/AtomicSimpleCPU.hh"
#include "sim/faults.hh"
#include "sim/system.hh"
#include "sim/full_system.hh"
#include "mem/abstract_mem.hh"
#include "mem/abstract_mem_meta.hh"
#include "cpu/reg_class.hh"
#include "debug/LimitStudy.hh"
#include "debug/SystemCall.hh"

using namespace std;
using namespace TheISA;

#define MAXNUMSYSCALLS 367

uint64_t raw_max;
uint64_t waw_max;
uint64_t war_max;
uint64_t mraw_max;
uint64_t mwaw_max;
uint64_t mwar_max;
uint64_t max_issue_cycle = 0;

uint64_t * reg_def_cycle;
uint64_t * reg_use_cycle;

std::string syscallList[400];

AtomicSimpleCPU::TickEvent::TickEvent(AtomicSimpleCPU *c)
    : Event(CPU_Tick_Pri), cpu(c)
{
}


void
AtomicSimpleCPU::TickEvent::process()
{
    cpu->tick();

    if(cpu->cycleOffset == -1) {
        cpu->cycleOffset = cpu->curCycle();

        DPRINTF(LimitStudyRegisterCycles, "cycleOffset = %lu\n", cpu->curCycle());

        //ignore this problem for now
        //cpu->cycleOffset = 0;

        DPRINTF(LimitStudyInstructions, "Cycle offset = %lu\n", cpu->cycleOffset);
    }
}

const char *
AtomicSimpleCPU::TickEvent::description() const
{
    return "AtomicSimpleCPU tick";
}

void
AtomicSimpleCPU::init()
{
    BaseCPU::init();

    reg_def_cycle = new uint64_t[TheISA::TotalNumRegs]();
    reg_use_cycle = new uint64_t[TheISA::TotalNumRegs]();

    // Initialise the ThreadContext's memory proxies
    tcBase()->initMemProxies(tcBase());

    if (FullSystem && !params()->switched_out) {
        ThreadID size = threadContexts.size();
        for (ThreadID i = 0; i < size; ++i) {
            ThreadContext *tc = threadContexts[i];
            // initialize CPU, including PC
            TheISA::initCPU(tc, tc->contextId());
        }
    }

    // Atomic doesn't do MT right now, so contextId == threadId
    ifetch_req.setThreadContext(_cpuId, 0); // Add thread ID if we add MT
    data_read_req.setThreadContext(_cpuId, 0); // Add thread ID here too
    data_write_req.setThreadContext(_cpuId, 0); // Add thread ID here too

}

AtomicSimpleCPU::AtomicSimpleCPU(AtomicSimpleCPUParams *p)
    : BaseSimpleCPU(p), tickEvent(this), width(p->width), locked(false),
      simulate_data_stalls(p->simulate_data_stalls),
      simulate_inst_stalls(p->simulate_inst_stalls),
      drain_manager(NULL),
      icachePort(name() + ".icache_port", this),
      dcachePort(name() + ".dcache_port", this),
      fastmem(p->fastmem),
      simpoint(p->simpoint_profile),
      intervalSize(p->simpoint_interval),
      intervalCount(0),
      intervalDrift(0),
      simpointStream(NULL),
      currentBBV(0, 0),
      currentBBVInstCount(0)
      /*ilpLimitStudy(p->ilpLimitStudy)*/
{
    _status = Idle;

    if (simpoint) {
        simpointStream = simout.create(p->simpoint_profile_file, false);
    }

    cycleOffset = -1;

    syscallList[0] = "restart_syscall";
    syscallList[1] = "exit";
    syscallList[2] = "fork";
    syscallList[3] = "read";
    syscallList[4] = "write";
    syscallList[5] = "open";
    syscallList[6] = "close";
    syscallList[8] = "creat";
    syscallList[9] = "link";
    syscallList[10] = "unlink";
    syscallList[11] = "execve";
    syscallList[12] = "chdir";
    syscallList[13] = "time";
    syscallList[14] = "mknod";
    syscallList[15] = "chmod";
    syscallList[16] = "lchown";
    syscallList[19] = "lseek";
    syscallList[20] = "getpid";
    syscallList[21] = "mount";
    syscallList[22] = "umount";
    syscallList[23] = "setuid";
    syscallList[24] = "getuid";
    syscallList[25] = "stime";
    syscallList[26] = "ptrace";
    syscallList[27] = "alarm";
    syscallList[29] = "pause";
    syscallList[30] = "utime";
    syscallList[33] = "access";
    syscallList[34] = "nice";
    syscallList[36] = "sync";
    syscallList[37] = "kill";
    syscallList[38] = "rename";
    syscallList[39] = "mkdir";
    syscallList[40] = "rmdir";
    syscallList[41] = "dup";
    syscallList[42] = "pipe";
    syscallList[43] = "times";
    syscallList[45] = "brk";
    syscallList[46] = "setgid";
    syscallList[47] = "getgid";
    syscallList[49] = "geteuid";
    syscallList[50] = "getegid";
    syscallList[51] = "acct";
    syscallList[52] = "umount2";
    syscallList[54] = "ioctl";
    syscallList[55] = "fcntl";
    syscallList[57] = "setpgid";
    syscallList[60] = "umask";
    syscallList[61] = "chroot";
    syscallList[62] = "ustat";
    syscallList[63] = "dup2";
    syscallList[64] = "getppid";
    syscallList[65] = "getpgrp";
    syscallList[66] = "setsid";
    syscallList[67] = "sigaction";
    syscallList[70] = "setreuid";
    syscallList[71] = "setregid";
    syscallList[72] = "sigsuspend";
    syscallList[73] = "sigpending";
    syscallList[74] = "sethostname";
    syscallList[75] = "setrlimit";
    syscallList[76] = "getrlimit";
    syscallList[77] = "getrusage";
    syscallList[78] = "gettimeofday";
    syscallList[79] = "settimeofday";
    syscallList[80] = "getgroups";
    syscallList[81] = "setgroups";
    syscallList[82] = "select";
    syscallList[83] = "symlink";
    syscallList[85] = "readlink";
    syscallList[86] = "uselib";
    syscallList[87] = "swapon";
    syscallList[88] = "reboot";
    syscallList[89] = "readdir";
    syscallList[90] = "mmap";
    syscallList[91] = "munmap";
    syscallList[92] = "truncate";
    syscallList[93] = "ftruncate";
    syscallList[94] = "fchmod";
    syscallList[95] = "fchown";
    syscallList[96] = "getpriority";
    syscallList[97] = "setpriority";
    syscallList[99] = "statfs";
    syscallList[100] = "fstatfs";
    syscallList[102] = "socketcall";
    syscallList[103] = "syslog";
    syscallList[104] = "setitimer";
    syscallList[105] = "getitimer";
    syscallList[106] = "stat";
    syscallList[107] = "lstat";
    syscallList[108] = "fstat";
    syscallList[111] = "vhangup";
    syscallList[113] = "syscall";
    syscallList[114] = "wait4";
    syscallList[115] = "swapoff";
    syscallList[116] = "sysinfo";
    syscallList[117] = "ipc";
    syscallList[118] = "fsync";
    syscallList[119] = "sigreturn";
    syscallList[120] = "clone";
    syscallList[121] = "setdomainname";
    syscallList[122] = "uname";
    syscallList[124] = "adjtimex";
    syscallList[125] = "mprotect";
    syscallList[126] = "sigprocmask";
    syscallList[128] = "init_module";
    syscallList[129] = "delete_module";
    syscallList[131] = "quotactl";
    syscallList[132] = "getpgid";
    syscallList[133] = "fchdir";
    syscallList[134] = "bdflush";
    syscallList[135] = "sysfs";
    syscallList[136] = "personality";
    syscallList[138] = "setfsuid";
    syscallList[139] = "setfsgid";
    syscallList[140] = "_llseek";
    syscallList[141] = "getdents";
    syscallList[142] = "_newselect";
    syscallList[143] = "flock";
    syscallList[144] = "msync";
    syscallList[145] = "readv";
    syscallList[146] = "writev";
    syscallList[147] = "getsid";
    syscallList[148] = "fdatasync";
    syscallList[149] = "_sysctl";
    syscallList[150] = "mlock";
    syscallList[151] = "munlock";
    syscallList[152] = "mlockall";
    syscallList[153] = "munlockall";
    syscallList[154] = "sched_setparam";
    syscallList[155] = "sched_getparam";
    syscallList[156] = "sched_setscheduler";
    syscallList[157] = "sched_getscheduler";
    syscallList[158] = "sched_yield";
    syscallList[159] = "sched_get_priority_max";
    syscallList[160] = "sched_get_priority_min";
    syscallList[161] = "sched_rr_get_interval";
    syscallList[162] = "nanosleep";
    syscallList[163] = "mremap";
    syscallList[164] = "setresuid";
    syscallList[165] = "getresuid";
    syscallList[168] = "poll";
    syscallList[169] = "nfsservctl";
    syscallList[170] = "setresgid";
    syscallList[171] = "getresgid";
    syscallList[172] = "prctl";
    syscallList[173] = "rt_sigreturn";
    syscallList[174] = "rt_sigaction";
    syscallList[175] = "rt_sigprocmask";
    syscallList[176] = "rt_sigpending";
    syscallList[177] = "rt_sigtimedwait";
    syscallList[178] = "rt_sigqueueinfo";
    syscallList[179] = "rt_sigsuspend";
    syscallList[180] = "pread64";
    syscallList[181] = "pwrite64";
    syscallList[182] = "chown";
    syscallList[183] = "getcwd";
    syscallList[184] = "capget";
    syscallList[185] = "capset";
    syscallList[186] = "sigaltstack";
    syscallList[187] = "sendfile";
    syscallList[190] = "vfork";
    syscallList[191] = "ugetrlimit";
    syscallList[192] = "mmap2";
    syscallList[193] = "truncate64";
    syscallList[194] = "ftruncate64";
    syscallList[195] = "stat64";
    syscallList[196] = "lstat64";
    syscallList[197] = "fstat64";
    syscallList[198] = "lchown32";
    syscallList[199] = "getuid32";
    syscallList[200] = "getgid32";
    syscallList[201] = "geteuid32";
    syscallList[202] = "getegid32";
    syscallList[203] = "setreuid32";
    syscallList[204] = "setregid32";
    syscallList[205] = "getgroups32";
    syscallList[206] = "setgroups32";
    syscallList[207] = "fchown32";
    syscallList[208] = "setresuid32";
    syscallList[209] = "getresuid32";
    syscallList[210] = "setresgid32";
    syscallList[211] = "getresgid32";
    syscallList[212] = "chown32";
    syscallList[213] = "setuid32";
    syscallList[214] = "setgid32";
    syscallList[215] = "setfsuid32";
    syscallList[216] = "setfsgid32";
    syscallList[217] = "getdents64";
    syscallList[218] = "pivot_root";
    syscallList[219] = "mincore";
    syscallList[220] = "madvise";
    syscallList[221] = "fcntl64";
    syscallList[224] = "gettid";
    syscallList[225] = "readahead";
    syscallList[226] = "setxattr";
    syscallList[227] = "lsetxattr";
    syscallList[228] = "fsetxattr";
    syscallList[229] = "getxattr";
    syscallList[230] = "lgetxattr";
    syscallList[231] = "fgetxattr";
    syscallList[232] = "listxattr";
    syscallList[233] = "llistxattr";
    syscallList[234] = "flistxattr";
    syscallList[235] = "removexattr";
    syscallList[236] = "lremovexattr";
    syscallList[237] = "fremovexattr";
    syscallList[238] = "tkill";
    syscallList[239] = "sendfile64";
    syscallList[240] = "futex";
    syscallList[241] = "sched_setaffinity";
    syscallList[242] = "sched_getaffinity";
    syscallList[243] = "io_setup";
    syscallList[244] = "io_destroy";
    syscallList[245] = "io_getevents";
    syscallList[246] = "io_submit";
    syscallList[247] = "io_cancel";
    syscallList[248] = "exit_group";
    syscallList[249] = "lookup_dcookie";
    syscallList[250] = "epoll_create";
    syscallList[251] = "epoll_ctl";
    syscallList[252] = "epoll_wait";
    syscallList[253] = "remap_file_pages";
    syscallList[256] = "set_tid_address";
    syscallList[257] = "timer_create";
    syscallList[258] = "timer_settime";
    syscallList[259] = "timer_gettime";
    syscallList[260] = "timer_getoverrun";
    syscallList[261] = "timer_delete";
    syscallList[262] = "clock_settime";
    syscallList[263] = "clock_gettime";
    syscallList[264] = "clock_getres";
    syscallList[265] = "clock_nanosleep";
    syscallList[266] = "statfs64";
    syscallList[267] = "fstatfs64";
    syscallList[268] = "tgkill";
    syscallList[269] = "utimes";
    syscallList[270] = "arm_fadvise64_64";
    syscallList[271] = "pciconfig_iobase";
    syscallList[272] = "pciconfig_read";
    syscallList[273] = "pciconfig_write";
    syscallList[274] = "mq_open";
    syscallList[275] = "mq_unlink";
    syscallList[276] = "mq_timedsend";
    syscallList[277] = "mq_timedreceive";
    syscallList[278] = "mq_notify";
    syscallList[279] = "mq_getsetattr";
    syscallList[280] = "waitid";
    syscallList[281] = "socket";
    syscallList[282] = "bind";
    syscallList[283] = "connect";
    syscallList[284] = "listen";
    syscallList[285] = "accept";
    syscallList[286] = "getsockname";
    syscallList[287] = "getpeername";
    syscallList[288] = "socketpair";
    syscallList[289] = "send";
    syscallList[290] = "sendto";
    syscallList[291] = "recv";
    syscallList[292] = "recvfrom";
    syscallList[293] = "shutdown";
    syscallList[294] = "setsockopt";
    syscallList[295] = "getsockopt";
    syscallList[296] = "sendmsg";
    syscallList[297] = "recvmsg";
    syscallList[298] = "semop";
    syscallList[299] = "semget";
    syscallList[300] = "semctl";
    syscallList[301] = "msgsnd";
    syscallList[302] = "msgrcv";
    syscallList[303] = "msgget";
    syscallList[304] = "msgctl";
    syscallList[305] = "shmat";
    syscallList[306] = "shmdt";
    syscallList[307] = "shmget";
    syscallList[308] = "shmctl";
    syscallList[309] = "add_key";
    syscallList[310] = "request_key";
    syscallList[311] = "keyctl";
    syscallList[312] = "semtimedop";
    syscallList[313] = "vserver";
    syscallList[314] = "ioprio_set";
    syscallList[315] = "ioprio_get";
    syscallList[316] = "inotify_init";
    syscallList[317] = "inotify_add_watch";
    syscallList[318] = "inotify_rm_watch";
    syscallList[319] = "mbind";
    syscallList[320] = "get_mempolicy";
    syscallList[321] = "set_mempolicy";
    syscallList[322] = "openat";
    syscallList[323] = "mkdirat";
    syscallList[324] = "mknodat";
    syscallList[325] = "fchownat";
    syscallList[326] = "futimesat";
    syscallList[327] = "fstatat64";
    syscallList[328] = "unlinkat";
    syscallList[329] = "renameat";
    syscallList[330] = "linkat";
    syscallList[331] = "symlinkat";
    syscallList[332] = "readlinkat";
    syscallList[333] = "fchmodat";
    syscallList[334] = "faccessat";
    syscallList[335] = "pselect6";
    syscallList[336] = "ppoll";
    syscallList[337] = "unshare";
    syscallList[338] = "set_robust_list";
    syscallList[339] = "get_robust_list";
    syscallList[340] = "splice";
    syscallList[341] = "arm_sync_file_range";
    syscallList[342] = "tee";
    syscallList[343] = "vmsplice";
    syscallList[344] = "move_pages";
    syscallList[345] = "getcpu";
    syscallList[346] = "epoll_pwait";
    syscallList[347] = "kexec_load";
    syscallList[348] = "utimensat";
    syscallList[349] = "signalfd";
    syscallList[350] = "timerfd_create";
    syscallList[351] = "eventfd";
    syscallList[352] = "fallocate";
    syscallList[353] = "timerfd_settime";
    syscallList[354] = "timerfd_gettime";
    syscallList[355] = "signalfd4";
    syscallList[356] = "eventfd2";
    syscallList[357] = "epoll_create1";
    syscallList[358] = "dup3";
    syscallList[359] = "pipe2";
    syscallList[360] = "inotify_init1";
    syscallList[361] = "preadv";
    syscallList[362] = "pwritev";
    syscallList[363] = "rt_tgsigqueueinfo";
    syscallList[364] = "perf_event_open";
    syscallList[365] = "recvmmsg";
    syscallList[366] = "accept4";    
}


AtomicSimpleCPU::~AtomicSimpleCPU()
{
    if (tickEvent.scheduled()) {
        deschedule(tickEvent);
    }
    if (simpointStream) {
        simout.close(simpointStream);
    }

    delete reg_def_cycle;
    delete reg_use_cycle;
}

unsigned int
AtomicSimpleCPU::drain(DrainManager *dm)
{
    assert(!drain_manager);
    if (switchedOut())
        return 0;

    if (!isDrained()) {
        DPRINTF(Drain, "Requesting drain: %s\n", pcState());
        drain_manager = dm;
        return 1;
    } else {
        if (tickEvent.scheduled())
            deschedule(tickEvent);

        DPRINTF(Drain, "Not executing microcode, no need to drain.\n");
        return 0;
    }
}

void
AtomicSimpleCPU::drainResume()
{
    assert(!tickEvent.scheduled());
    assert(!drain_manager);
    if (switchedOut())
        return;

    DPRINTF(SimpleCPU, "Resume\n");
    verifyMemoryMode();

    assert(!threadContexts.empty());
    if (threadContexts.size() > 1)
        fatal("The atomic CPU only supports one thread.\n");

    if (thread->status() == ThreadContext::Active) {
        schedule(tickEvent, nextCycle());
        _status = BaseSimpleCPU::Running;
        notIdleFraction = 1;
    } else {
        _status = BaseSimpleCPU::Idle;
        notIdleFraction = 0;
    }

    system->totalNumInsts = 0;
}

bool
AtomicSimpleCPU::tryCompleteDrain()
{
    if (!drain_manager)
        return false;

    DPRINTF(Drain, "tryCompleteDrain: %s\n", pcState());
    if (!isDrained())
        return false;

    DPRINTF(Drain, "CPU done draining, processing drain event\n");
    drain_manager->signalDrainDone();
    drain_manager = NULL;

    return true;
}


void
AtomicSimpleCPU::switchOut()
{
    BaseSimpleCPU::switchOut();

    assert(!tickEvent.scheduled());
    assert(_status == BaseSimpleCPU::Running || _status == Idle);
    assert(isDrained());
}


void
AtomicSimpleCPU::takeOverFrom(BaseCPU *oldCPU)
{
    BaseSimpleCPU::takeOverFrom(oldCPU);

    // The tick event should have been descheduled by drain()
    assert(!tickEvent.scheduled());

    ifetch_req.setThreadContext(_cpuId, 0); // Add thread ID if we add MT
    data_read_req.setThreadContext(_cpuId, 0); // Add thread ID here too
    data_write_req.setThreadContext(_cpuId, 0); // Add thread ID here too
}

void
AtomicSimpleCPU::verifyMemoryMode() const
{
    if (!system->isAtomicMode()) {
        fatal("The atomic CPU requires the memory system to be in "
              "'atomic' mode.\n");
    }
}

void
AtomicSimpleCPU::activateContext(ThreadID thread_num, Cycles delay)
{
    DPRINTF(SimpleCPU, "ActivateContext %d (%d cycles)\n", thread_num, delay);

    assert(thread_num == 0);
    assert(thread);

    assert(_status == Idle);
    assert(!tickEvent.scheduled());

    notIdleFraction = 1;
    numCycles += ticksToCycles(thread->lastActivate - thread->lastSuspend);

    //Make sure ticks are still on multiples of cycles
    schedule(tickEvent, clockEdge(delay));
    _status = BaseSimpleCPU::Running;
}


void
AtomicSimpleCPU::suspendContext(ThreadID thread_num)
{
    DPRINTF(SimpleCPU, "SuspendContext %d\n", thread_num);

    assert(thread_num == 0);
    assert(thread);

    if (_status == Idle)
        return;

    assert(_status == BaseSimpleCPU::Running);

    // tick event may not be scheduled if this gets called from inside
    // an instruction's execution, e.g. "quiesce"
    if (tickEvent.scheduled())
        deschedule(tickEvent);

    notIdleFraction = 0;
    _status = Idle;
}


Fault
AtomicSimpleCPU::readMem(Addr addr, uint8_t * data,
                         unsigned size, unsigned flags)
{
    // use the CPU's statically allocated read request and packet objects
    Request *req = &data_read_req;

    if (traceData) {
        traceData->setAddr(addr);
    }

    //The size of the data we're trying to read.
    int fullSize = size;

    //The address of the second part of this access if it needs to be split
    //across a cache line boundary.
    Addr secondAddr = roundDown(addr + size - 1, cacheLineSize());

    if (secondAddr > addr)
        size = secondAddr - addr;

    dcache_latency = 0;

    while (1) {
        req->setVirt(0, addr, size, flags, dataMasterId(), thread->pcState().instAddr());

        // translate to physical address
        Fault fault = thread->dtb->translateAtomic(req, tc, BaseTLB::Read);

        // Now do the access.
        if (fault == NoFault && !req->getFlags().isSet(Request::NO_ACCESS)) {
            Packet pkt = Packet(req,
                                req->isLLSC() ? MemCmd::LoadLockedReq :
                                MemCmd::ReadReq);
            pkt.dataStatic(data);

            if (req->isMmappedIpr())
                dcache_latency += TheISA::handleIprRead(thread->getTC(), &pkt);
            else {
                if (fastmem && system->isMemAddr(pkt.getAddr())) {
                    system->getPhysMem().access(&pkt);

                    if(system->ilpLimitStudy != "none" && inUserMode(tc)) {
                        abstractMemory = system->getPhysMem().getAbstractMemory(&pkt);
                        abstractMemory->metaPointers.init(abstractMemory->size());
                        abstractMemory->metaPointers.initMetaData(currentOffset, pkt.getAddr());
                        currentOffset = pkt.getAddr() - abstractMemory->start();
                        currentAddress = pkt.getAddr();
                        mraw_max = abstractMemory->metaPointers.getMemDefCycle(currentOffset);
                        DPRINTF(LimitStudyMemory, "getMemDefCycle(%lu) = cycle %lu\n", currentOffset, mraw_max);
                    }
                }
                else
                    dcache_latency += dcachePort.sendAtomic(&pkt);
            }
            dcache_access = true;

            assert(!pkt.isError());

            if (req->isLLSC()) {
                TheISA::handleLockedRead(thread, req);
            }
        }

        //If there's a fault, return it
        if (fault != NoFault) {
            if (req->isPrefetch()) {
                return NoFault;
            } else {
                return fault;
            }
        }

        //If we don't need to access a second cache line, stop now.
        if (secondAddr <= addr)
        {
            if (req->isLocked() && fault == NoFault) {
                assert(!locked);
                locked = true;
            }
            return fault;
        }

        /*
         * Set up for accessing the second cache line.
         */

        //Move the pointer we're reading into to the correct location.
        data += size;
        //Adjust the size to get the remaining bytes.
        size = addr + fullSize - secondAddr;
        //And access the right address.
        addr = secondAddr;
    }
}


Fault
AtomicSimpleCPU::writeMem(uint8_t *data, unsigned size,
                          Addr addr, unsigned flags, uint64_t *res)
{
    // use the CPU's statically allocated write request and packet objects
    Request *req = &data_write_req;

    if (traceData) {
        traceData->setAddr(addr);
    }

    //The size of the data we're trying to read.
    int fullSize = size;

    //The address of the second part of this access if it needs to be split
    //across a cache line boundary.
    Addr secondAddr = roundDown(addr + size - 1, cacheLineSize());

    if(secondAddr > addr)
        size = secondAddr - addr;

    dcache_latency = 0;

    while(1) {
        req->setVirt(0, addr, size, flags, dataMasterId(), thread->pcState().instAddr());

        // translate to physical address
        Fault fault = thread->dtb->translateAtomic(req, tc, BaseTLB::Write);

        // Now do the access.
        if (fault == NoFault) {
            MemCmd cmd = MemCmd::WriteReq; // default
            bool do_access = true;  // flag to suppress cache access

            if (req->isLLSC()) {
                cmd = MemCmd::StoreCondReq;
                do_access = TheISA::handleLockedWrite(thread, req);
            } else if (req->isSwap()) {
                cmd = MemCmd::SwapReq;
                if (req->isCondSwap()) {
                    assert(res);
                    req->setExtraData(*res);
                }
            }

            if (do_access && !req->getFlags().isSet(Request::NO_ACCESS)) {
                Packet pkt = Packet(req, cmd);
                pkt.dataStatic(data);

                if (req->isMmappedIpr()) {
                    dcache_latency +=
                        TheISA::handleIprWrite(thread->getTC(), &pkt);
                } else {
                    if (fastmem && system->isMemAddr(pkt.getAddr())) {
                        system->getPhysMem().access(&pkt);

                        if(system->ilpLimitStudy != "none" && inUserMode(tc)) {
                            abstractMemory = system->getPhysMem().getAbstractMemory(&pkt);
                            abstractMemory->metaPointers.init(abstractMemory->size());
                            abstractMemory->metaPointers.initMetaData(currentOffset, pkt.getAddr());
                            currentOffset = pkt.getAddr() - abstractMemory->start();
                            currentAddress = pkt.getAddr();
                            mwar_max = abstractMemory->metaPointers.getMemUseCycle(currentOffset);
                            mwaw_max = abstractMemory->metaPointers.getMemDefCycle(currentOffset);
                            DPRINTF(LimitStudyMemory, "getMemDefCycle(%lu) = cycle %lu\n", currentOffset, mwaw_max);
                            DPRINTF(LimitStudyMemory, "getMemUseCycle(%lu) = cycle %lu\n", currentOffset, mwar_max);
                        }
                    }
                    else
                        dcache_latency += dcachePort.sendAtomic(&pkt);
                }
                dcache_access = true;
                assert(!pkt.isError());

                if (req->isSwap()) {
                    assert(res);
                    memcpy(res, pkt.getPtr<uint8_t>(), fullSize);
                }
            }

            if (res && !req->isSwap()) {
                *res = req->getExtraData();
            }
        }

        //If there's a fault or we don't need to access a second cache line,
        //stop now.
        if (fault != NoFault || secondAddr <= addr)
        {
            if (req->isLocked() && fault == NoFault) {
                assert(locked);
                locked = false;
            }
            if (fault != NoFault && req->isPrefetch()) {
                return NoFault;
            } else {
                return fault;
            }
        }

        /*
         * Set up for accessing the second cache line.
         */

        //Move the pointer we're reading into to the correct location.
        data += size;
        //Adjust the size to get the remaining bytes.
        size = addr + fullSize - secondAddr;
        //And access the right address.
        addr = secondAddr;
    }
}


void
AtomicSimpleCPU::tick()
{
    DPRINTF(SimpleCPU, "Tick\n");

    Tick latency = 0;

    raw_max = 0;  
    war_max = 0;
    waw_max = 0;
    mraw_max = 0;  
    mwar_max = 0;
    mwaw_max = 0;
    uint16_t dest_reg;
    uint16_t src_reg;

    for (int i = 0; i < width || locked; ++i) {
        numCycles++;

        if(inUserMode(tc)) {
            numUserCycles++;
        }

        if (!curStaticInst || !curStaticInst->isDelayedCommit())
            checkForInterrupts();

        checkPcEventQueue();
        // We must have just got suspended by a PC event
        if (_status == Idle) {
            tryCompleteDrain();
            return;
        }

        Fault fault = NoFault;

        TheISA::PCState pcState = thread->pcState();

        bool needToFetch = !isRomMicroPC(pcState.microPC()) &&
                           !curMacroStaticInst;
        if (needToFetch) {
            setupFetchRequest(&ifetch_req);
            fault = thread->itb->translateAtomic(&ifetch_req, tc,
                                                 BaseTLB::Execute);
        }

        if (fault == NoFault) {
            Tick icache_latency = 0;
            bool icache_access = false;
            dcache_access = false; // assume no dcache access

            if (needToFetch) {
                // This is commented out because the decoder would act like
                // a tiny cache otherwise. It wouldn't be flushed when needed
                // like the I cache. It should be flushed, and when that works
                // this code should be uncommented.
                //Fetch more instruction memory if necessary
                //if(decoder.needMoreBytes())
                //{
                    icache_access = true;
                    Packet ifetch_pkt = Packet(&ifetch_req, MemCmd::ReadReq);
                    ifetch_pkt.dataStatic(&inst);

                    if (fastmem && system->isMemAddr(ifetch_pkt.getAddr()))
                        system->getPhysMem().access(&ifetch_pkt);
                    else
                        icache_latency = icachePort.sendAtomic(&ifetch_pkt);

                    assert(!ifetch_pkt.isError());

                    // ifetch_req is initialized to read the instruction directly
                    // into the CPU object's inst field.
                //}
            }

            preExecute();

            if(curStaticInst) {

                fault = curStaticInst->execute(this, traceData);

                // keep an instruction count
                if (fault == NoFault)
                    countInst();
                else if (traceData && !DTRACE(ExecFaulting)) {
                    delete traceData;
                    traceData = NULL;
                }

                // the last two boolean expressions ensures that a single instruction is only accounted for once here,
                // since a single instruction may consist of several micro ops
                if(inUserMode(tc) && system->ilpLimitStudy != "none" && fault == NoFault && (!curStaticInst->isMicroop() || curStaticInst->isLastMicroop()))
                // remember to mirror this if statement after postExecute() below as well
                {

                    DPRINTF(LimitStudyInstructions, "inst_num_counter = %lu\n", curStaticInst->inst_num_counter);

                    curStaticInst->inst_num = curStaticInst->inst_num_counter;
                    curStaticInst->inst_num_counter++;

                    for(int i = 0; i < curStaticInst->numSrcRegs(); i++)
                    {
                        src_reg = getFlatRegIdx(curStaticInst->srcRegIdx(i));

                        raw_max = std::max(raw_max, reg_def_cycle[src_reg]);

                        DPRINTF(LimitStudyRegisters, "reg_def_cycle[%d] = %lu\n", src_reg, reg_def_cycle[src_reg]);
                    }

                    for(int i = 0; i < curStaticInst->numDestRegs(); i++)
                    {
                        dest_reg = getFlatRegIdx(curStaticInst->destRegIdx(i));
                                   
                        war_max = std::max(war_max, reg_use_cycle[dest_reg]);
                        waw_max = std::max(waw_max, reg_def_cycle[dest_reg]);

                        DPRINTF(LimitStudyRegisters, "reg_use_cycle[%d] = %lu\n", dest_reg, reg_def_cycle[dest_reg]);
                        DPRINTF(LimitStudyRegisters, "reg_def_cycle[%d] = %lu\n", dest_reg, reg_use_cycle[dest_reg]);
                    }

                    if(system->ilpLimitStudy == "register") {
                        curStaticInst->issue_cycle = std::max(raw_max, std::max(war_max, std::max(waw_max, std::max(mraw_max, std::max(mwar_max, mwaw_max)))));
                    }
                    else if(system->ilpLimitStudy == "memory") {
                        curStaticInst->issue_cycle = std::max(raw_max, 
                                                              std::max((const long unsigned int) 0,
                                                              std::max((const long unsigned int ) 0, 
                                                              std::max(mraw_max, 
                                                              std::max(mwar_max, mwaw_max)))));
                    }

                    if(system->ilpLimitStudy != "none") {

                        if(curStaticInst->issue_cycle > max_issue_cycle) {

                            uint64_t lostCycles = curStaticInst->issue_cycle - max_issue_cycle;

                            curStaticInst->pc = thread->pcState().instAddr();

                            if(curStaticInst->inst_num < sLostCycles.size()) {
                                sLostCycles[curStaticInst->inst_num] += lostCycles;
                            }

                            DPRINTF(LimitStudyTrueDependencies, "\tissue cycle +%d,\t caused by %s \n",
                                lostCycles, 
                                curStaticInst->disassemble(thread->pcState().instAddr(), debugSymbolTable)); 

                            max_issue_cycle = curStaticInst->issue_cycle;
                        }
                    }
                }

                postExecute();

                /*
                 * This block tries to capture system calls that are being made. Have to make sure that
                 * curStaticInst is not null, that we're in the right mode, and that we have the right
                 * process. 
                 * Note the --sim-user-mode flag
                 */
                if(curStaticInst && system->simThisMode(tc) && system->simThisProcess(tc)) {

                    // get the corresponding assembly code for this instruction
                    std::string assembly = curStaticInst->disassemble(thread->pcState().instAddr(), debugSymbolTable);

                    // try to find it
                    std::size_t found = assembly.find("svc");

                    if(found != std::string::npos) {

                        // before the svc instruction, it is my understanding that the system call id
                        // is stored into reg7 on ARM, after which svc is called. so that this point
                        // reg7 should already have the syscall id. see arch/arm/include/asm/unistd.h
                        int index = tc->readIntReg(INTREG_R7);

                        if(index < MAXNUMSYSCALLS) {

                            histSystemCalls.sample(index);
                            DPRINTF(SystemCall, "%s making system call to %s [syscall #%d]\n", system->getPname(tc), syscallList[index], index);
                        }
                    }
                }


                if(inUserMode(tc) && system->ilpLimitStudy != "none" && fault == NoFault && (!curStaticInst->isMicroop() || curStaticInst->isLastMicroop())) {

                    for(int i = 0; i < curStaticInst->numDestRegs(); i++)
                    {
                        dest_reg = getFlatRegIdx(curStaticInst->destRegIdx(i));

                        reg_def_cycle[dest_reg] = (uint64_t) numUserCycles.value() + 1;

                        DPRINTF(LimitStudyRegisterCycles, "reg_def_cycle[%d] = %lu\n", (int) dest_reg, reg_def_cycle[dest_reg]);
                    }
                
                    for(int i = 0; i < curStaticInst->numSrcRegs(); i++)
                    {
                        src_reg = getFlatRegIdx(curStaticInst->srcRegIdx(i));

                            reg_use_cycle[src_reg] = (uint64_t) numUserCycles.value();

                        DPRINTF(LimitStudyRegisterCycles, "reg_use_cycle[%d] = %lu\n", (int) src_reg, reg_use_cycle[src_reg]);
                    }

                    if(curStaticInst->isStore())
                    {
                        abstractMemory->metaPointers.initMetaData(currentOffset, currentAddress);
                        abstractMemory->metaPointers.setMemDefCycle(currentOffset, (uint64_t) numUserCycles.value() + 1);

                        DPRINTF(LimitStudyMemoryCycles, "mem_def_cycle[%lu] = %lu\n", currentOffset, (uint64_t) numUserCycles.value() + 1);
                    }
                    else if(curStaticInst->isLoad())
                    {
                        abstractMemory->metaPointers.initMetaData(currentOffset, currentAddress);
                        abstractMemory->metaPointers.setMemUseCycle(currentOffset, (uint64_t) numUserCycles.value());

                        DPRINTF(LimitStudyMemoryCycles, "mem_use_cycle[%lu] = %lu\n", currentOffset, (uint64_t) numUserCycles.value());
                    }
                }
            }

            // @todo remove me after debugging with legion done
            if (curStaticInst && (!curStaticInst->isMicroop() ||
                        curStaticInst->isFirstMicroop()))
                instCnt++;

            // profile for SimPoints if enabled and macro inst is finished
            if (simpoint && curStaticInst && (fault == NoFault) &&
                    (!curStaticInst->isMicroop() ||
                     curStaticInst->isLastMicroop())) {
                profileSimPoint();
            }

            Tick stall_ticks = 0;
            if (simulate_inst_stalls && icache_access)
                stall_ticks += icache_latency;

            if (simulate_data_stalls && dcache_access)
                stall_ticks += dcache_latency;

            if (stall_ticks) {
                // the atomic cpu does its accounting in ticks, so
                // keep counting in ticks but round to the clock
                // period
                latency += divCeil(stall_ticks, clockPeriod()) *
                    clockPeriod();
            }

        }
        if(fault != NoFault || !stayAtPC)
            advancePC(fault);
    }

    if (tryCompleteDrain())
        return;

    // instruction takes at least one cycle
    if (latency < clockPeriod())
        latency = clockPeriod();

    if (_status != Idle)
        schedule(tickEvent, curTick() + latency);
}


void
AtomicSimpleCPU::printAddr(Addr a)
{
    dcachePort.printAddr(a);
}

void
AtomicSimpleCPU::profileSimPoint()
{
    if (!currentBBVInstCount)
        currentBBV.first = thread->pcState().instAddr();

    ++intervalCount;
    ++currentBBVInstCount;

    // If inst is control inst, assume end of basic block.
    if (curStaticInst->isControl()) {
        currentBBV.second = thread->pcState().instAddr();

        auto map_itr = bbMap.find(currentBBV);
        if (map_itr == bbMap.end()){
            // If a new (previously unseen) basic block is found,
            // add a new unique id, record num of insts and insert into bbMap.
            BBInfo info;
            info.id = bbMap.size() + 1;
            info.insts = currentBBVInstCount;
            info.count = currentBBVInstCount;
            bbMap.insert(std::make_pair(currentBBV, info));
        } else {
            // If basic block is seen before, just increment the count by the
            // number of insts in basic block.
            BBInfo& info = map_itr->second;
            assert(info.insts == currentBBVInstCount);
            info.count += currentBBVInstCount;
        }
        currentBBVInstCount = 0;

        // Reached end of interval if the sum of the current inst count
        // (intervalCount) and the excessive inst count from the previous
        // interval (intervalDrift) is greater than/equal to the interval size.
        if (intervalCount + intervalDrift >= intervalSize) {
            // summarize interval and display BBV info
            std::vector<pair<uint64_t, uint64_t> > counts;
            for (auto map_itr = bbMap.begin(); map_itr != bbMap.end();
                    ++map_itr) {
                BBInfo& info = map_itr->second;
                if (info.count != 0) {
                    counts.push_back(std::make_pair(info.id, info.count));
                    info.count = 0;
                }
            }
            std::sort(counts.begin(), counts.end());

            // Print output BBV info
            *simpointStream << "T";
            for (auto cnt_itr = counts.begin(); cnt_itr != counts.end();
                    ++cnt_itr) {
                *simpointStream << ":" << cnt_itr->first
                                << ":" << cnt_itr->second << " ";
            }
            *simpointStream << "\n";

            intervalDrift = (intervalCount + intervalDrift) - intervalSize;
            intervalCount = 0;
        }
    }
}

void
AtomicSimpleCPU::postExecute()
{
    BaseSimpleCPU::postExecute();

    if(system->ilpLimitStudy != "none") {

        // make sure we don't get any crazy numbers, issue cycle should be less than num cycles
        // inst num should be greater than 0 but less than inst num counter
        if(curStaticInst->issue_cycle < (uint64_t) numUserCycles.value() && 
           curStaticInst->inst_num > 0 &&
           curStaticInst->inst_num < curStaticInst->inst_num_counter) {


            if(curStaticInst->inst_num < instNumCycle.size() && curStaticInst->issue_cycle < instNumCycle.size()) {

                  instNumCycle[curStaticInst->inst_num] = curStaticInst->issue_cycle;
                  instNumPc[curStaticInst->inst_num] = curStaticInst->pc;

            }   

            DPRINTF(LimitStudyInstructions, "inst_num: %lu\t%s issue_cycle: %lu \tnumUserCycles: %lu (numCycles %lu)\n", 
                curStaticInst->inst_num, 
                curStaticInst->getCachedDisassembly(), 
                curStaticInst->issue_cycle, numUserCycles.value(), numCycles.value());
            
        }
    }
}

void AtomicSimpleCPU::regStats()
{
    BaseSimpleCPU::regStats();

    int executionProfileSize = system->ilpLimitStudy != "none" ? 10000000 : 1;

    numUserCycles
        .name(name() + ".numProcessCycles")
        .desc("Number of cycles elapsed for the process(es) being tracked")
        ;

    instNumCycle
        .init(executionProfileSize)
        .name(name() + ".instNumCycle")
        .desc("Earliest cycle in which this instruction can be executed")
        ;       

    instNumPc
        .init(executionProfileSize)
        .name(name() + ".instNumPc")
        .desc("Pc of this instruction")
        ;

    sLostCycles
        .init(executionProfileSize)
        .name(".lostCycles")
        .desc("Lost cycles due to this instruction")
        ;

    histSystemCalls
        // number of syscalls in the kernel source include/asc/unistd.h
        // comforting to know htat none of the deprecated syscalls in that file show up in the stats
        .init(370)
        .name(name() + ".histSystemCalls")
        .desc("Number of times this system call was made")
        ; 
}

////////////////////////////////////////////////////////////////////////
//
//  AtomicSimpleCPU Simulation Object
//
AtomicSimpleCPU *
AtomicSimpleCPUParams::create()
{
    numThreads = 1;
    if (!FullSystem && workload.size() != 1)
        panic("only one workload allowed");
    return new AtomicSimpleCPU(this);
}
